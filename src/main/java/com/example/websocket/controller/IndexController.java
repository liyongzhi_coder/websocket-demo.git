package com.example.websocket.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping("/index1")
    public String  index1 (){

        return "index1";
    }
    @GetMapping("/index2")
    public String  index2 (){

        return "index2";
    }

    @GetMapping("/h5")
    public String  h5 (){

        return "h5";
    }

    @GetMapping("/chatroom")
    public String  chatroom (){

        return "chatroom";
    }
}
