package com.example.websocket.controller;

import com.example.websocket.domain.User;
import com.example.websocket.service.MyHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.TextMessage;

@Controller
public class WebSocketController {

    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    private  MyHandler handler;

    //广播推送消息
    @Scheduled(fixedRate = 10000)
    public void sendTopicMessage() {
        System.out.println("后台广播推送！");
        User user = new User();
        user.setUserName("oyzc");
        user.setAge(10);
        this.template.convertAndSend("/topic/getResponse", user);
    }

    //一对一推送消息
    @Scheduled(fixedRate = 10000)
    public void sendQueueMessage() {
        System.out.println("后台一对一推送！");
        User user = new User();
        user.setUserId(1l);
        user.setUserName("oyzc");
        user.setAge(10);
        this.template.convertAndSendToUser(user.getUserId() + "", "/queue/getResponse", user);
    }


    @Scheduled(fixedRate = 10000)
    public void sendMessage() {
        boolean b = handler.sendMessageToUser("888",new TextMessage("服务器主动推送的一条信息ccc"));
        System.out.println(b);
    }

}
